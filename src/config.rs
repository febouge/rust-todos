use std::env;
use std::path::PathBuf;
use custom_error::CustomError;

pub const APP_PATH: &'static str = ".todos";
pub const DATABASE_FILE: &'static str = "database";

pub fn database_full_path() -> Result<PathBuf, CustomError> {
    if home_dir_is_not_defined() {
        return Err(CustomError::HomeNotDefined);
    }
    let mut complete_path: PathBuf = app_path().unwrap();
    complete_path.push(DATABASE_FILE);
    return Ok(complete_path);
}

pub fn app_path() -> Result<PathBuf, CustomError>  {
    if home_dir_is_not_defined() {
        return Err(CustomError::HomeNotDefined);
    }
    let mut complete_path: PathBuf = home_dir().unwrap();
    complete_path.push(APP_PATH);
    return Ok(complete_path);
}

pub fn home_dir_is_not_defined() -> bool {
    home_dir().is_none()
}

pub fn home_dir() -> Option<PathBuf> {
    env::home_dir()
}

pub fn relative_path() -> PathBuf {
    let mut path = PathBuf::from(APP_PATH);
    path.push(DATABASE_FILE);
    return path;
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn must_concat_correctly_the_relative_paths() {
        assert_eq!(PathBuf::from(".todos/database"), relative_path())
    }

    #[test]
    fn check_home_dir_existence() {
        assert!(!home_dir_is_not_defined())
    }

    #[test]
    fn absolute_path_is_properly_built() {

        let absolute_path = database_full_path().unwrap();
        let str_path = String::from(absolute_path.to_str().unwrap());
        assert!(absolute_path.is_absolute());
        assert!(str_path.contains("/.todos/database"));
    }

    #[test]
    fn should_return_app_folder_path() {
        let path = String::from(app_path().unwrap().to_str().unwrap());
        assert!(path.ends_with("/.todos"));
    }
}
