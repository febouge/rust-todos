use chrono::{Local, Date};
use std::fmt;
use std::string;

pub struct Task {
    done: bool,
    created_at: Date<Local>,
    description: string::String
}

impl Task {
    pub fn new(desc: &str) -> Task {
        Task{
            done: false,
            description: String::from(desc),
            created_at: Local::today()
        }
    }

    pub fn complete(&mut self) {
        self.done = true;
    }
}

impl fmt::Display for Task {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let check = if self.done { "✓" } else { " " };
        write!(f, "[{}] - {} - {}", check, self.created_at, self.description)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    fn get_task() -> Task {
        Task::new("Testing task")
    }

    #[test]
    fn done_must_be_false() {
        let task = get_task();
        assert!(!task.done);
    }

    #[test]
    fn description_must_have_test() {
        let task = get_task();
        assert_eq!("Testing task", task.description);
    }

    #[test]
    fn task_must_have_been_created_today() {
        let task = get_task();
        assert_eq!(Local::today(), task.created_at);
    }

    #[test]
    fn task_can_be_marked_as_done() {
        let mut task = get_task();
        task.complete();
        assert!(task.done);
    }
}
