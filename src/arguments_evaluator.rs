use options::*;
use task::Task;

const HELP_TEXT: &'static str = "Todo software written in Rust \n
Usage: \n
\t todos <option> <option-args> \n
Where <option> can be:
\t add \t\t -- Add task to the registry.
\t help \t\t -- Show this help.\n
";

pub fn evaluate_args(args: &Vec<String>) {
    let mut input_args: Vec<String> = args.clone();
    let secondary_args: Vec<String> = input_args.drain(1..).collect();
    let option_str = input_args.first().unwrap().to_lowercase();
    let option_selected: Options = Options::from(&option_str[..]);
    match option_selected {
        Options::Add => add(secondary_args),
        Options::Help => show_help(),
        Options::Unsupported(option) => show_param_error(option)
    }
}

pub fn show_help() {
    println!("{}", get_help_text());
}

fn add(add_options: Vec<String>) {
    println!("{}", "ADD option selected");
    let space = String::from(" ");
    let description: String = add_options.iter().fold(String::new(), |x, y| x + &space + y);
    let task = Task::new(description.trim());
    println!("Created task: {}", task);
}

fn show_param_error(first_option: String) {
    println!("Unknown option selected: {}", first_option);
    show_help();
}


fn get_help_text() -> String {
    return String::from(HELP_TEXT);
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn should_return_help() {
        assert_eq!(HELP_TEXT, get_help_text());
    }
}
