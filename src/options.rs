#[derive(Debug, PartialEq)]
pub enum Options {
    Add,
    Help,
    Unsupported(String)
}

impl<'a> From<&'a str> for Options {
    fn from(option_str: &'a str) -> Options {
        match option_str {
            "add" => Options::Add,
            "help" => Options::Help,
            _ => Options::Unsupported(option_str.to_string())
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn should_return_add() {
        assert_eq!(Options::Add, Options::from("add"));
    }

    #[test]
    fn should_return_help() {
        assert_eq!(Options::Help, Options::from("help"));
    }

    #[test]
    fn should_return_unsupported() {
        assert_eq!(Options::Unsupported(String::from("testingopt")), Options::from("testingopt"));
    }
}
