use std::fmt;
use std::error::Error;

const HOME_NOT_DEFINED_MSG: &'static str = "The environmental variable HOME is not defined";
const CORRPUTED_DATABASE_MSG: &'static str = "The database is corrupted";
const BAD_ARG_MSG: &'static str = "The argument introduced is not valid";
const CAN_NOT_PARSE_ARG_MSG: &'static str = "The arguments introduced can't be parsed";

#[derive(Debug)]
pub enum CustomError {
    HomeNotDefined,
    CorruptedDatabase,
    BadArgument,
    CanNotParseArguments
}

impl fmt::Display for CustomError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{:?} error: {}", self, self.description())
    }
}

impl Error for CustomError {
    fn description(&self) -> &str {
        let description = match self {
            &CustomError::HomeNotDefined => HOME_NOT_DEFINED_MSG,
            &CustomError::CorruptedDatabase => CORRPUTED_DATABASE_MSG,
            &CustomError::BadArgument => BAD_ARG_MSG,
            &CustomError::CanNotParseArguments => CAN_NOT_PARSE_ARG_MSG
        };
        return description;
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn home_not_defined_error_has_description() {
        assert_eq!(HOME_NOT_DEFINED_MSG, CustomError::HomeNotDefined.description())
    }

    #[test]
    fn corrupted_database_error_has_description() {
        assert_eq!(CORRPUTED_DATABASE_MSG, CustomError::CorruptedDatabase.description())
    }

    #[test]
    fn bad_argument_error_has_description() {
        assert_eq!(BAD_ARG_MSG, CustomError::BadArgument.description())
    }

    #[test]
    fn bad_arguments_error_has_description() {
        assert_eq!(BAD_ARG_MSG, CustomError::BadArgument.description())
    }
}
