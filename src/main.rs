extern crate chrono;

mod task;
mod arguments_parser;
mod arguments_evaluator;
mod options;
mod config;
mod custom_error;

fn main() {
    if config::home_dir_is_not_defined() {
        print_error(custom_error::CustomError::HomeNotDefined);
        return;
    }
    if arguments_parser::args_are_empty() {
        arguments_evaluator::show_help();
        return;
    }

    let parsed_arguments = arguments_parser::get_user_arguments();
    match parsed_arguments {
        Ok(arguments) =>  arguments_evaluator::evaluate_args(&arguments),
        Err(e) => print_error(e)
    }
    let mut task = task::Task::new("Install Rust");
    println!("{}", task);
    task.complete();
    println!("{}", task);
    print_error(custom_error::CustomError::HomeNotDefined);
}

fn print_error(error_message: custom_error::CustomError) {
    println!("{}", error_message);
}
