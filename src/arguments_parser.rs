use std::env;
use custom_error::CustomError;

pub fn args_are_empty() -> bool {
    valid_args_introduced()  == 0
}

pub fn get_user_arguments() -> Result<Vec<String>, CustomError>{
    let valid_args = valid_args_introduced();
    let user_valid_params = parse_args_introduced();
    if valid_args == user_valid_params.len() {
        Ok(user_valid_params)
    } else {
        Err(CustomError::CanNotParseArguments)
    }
}

fn valid_args_introduced() -> usize {
    env::args_os().len() - 1
}

fn parse_args_introduced() -> Vec<String> {
    let args = env::args_os();
    let mut parsed_args: Vec<String> = args.filter_map(|a| a.into_string().ok()).collect();
    parsed_args.remove(0);
    return parsed_args;
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn should_be_error(){
        assert!(get_user_arguments().is_ok());
    }

    #[test]
    fn args_length_must_be_zero(){
        assert_eq!(0, valid_args_introduced());
    }

    #[test]
    fn parsed_args_must_be_zero(){
        assert_eq!(0, parse_args_introduced().len());
    }

    #[test]
    fn introduced_args_must_be_empty(){
        assert!(args_are_empty());
    }
}
