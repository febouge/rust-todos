# ToDo management

This is an console app written in Rust to manage tasks. It has been created
with the purpose of learning Rust and do something useful for my day to day.

[![build status](https://gitlab.com/febouge/rust-todos/badges/master/build.svg)](https://gitlab.com/febouge/rust-todos/commits/master)


## Build and run the app

Assuming you have Rust and Cargo installed ([Instructions here](https://www.rust-lang.org/en-US/install.html)). Clone the project and build it with:

```
cargo build
```

If everything works fine, you run the app using:

```
cargo run
```
